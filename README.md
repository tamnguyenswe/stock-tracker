# Stock tracking & notification

The goal is to mimic trailing stop and manual stop orders. Since as of Feb 2024 VNSC doesn't have these 2 types of orders, this code will somewhat mimic the functionality of these orders. However it's not intended to be able to automatically create orders on VNSC, since VNSC requires 2FA for placing orders, and that would be a hassle to automate without an API.

## Todo list

- [x] Figuring how to mimic websocket connection to SSI.
- [x] Decode websocket data.
- [ ] 1st version, simple data structure:
  - [x] Pulled data storage:
    - [x] Stock symbol
    - [x] Highest price
    - [x] Highest price timestamp
    - [x] Count max price changes
  - [ ] User configuration:
    - [ ] Stock symbol
    - [ ] Trailing stop percentage
    - [ ] Manual stop price
- [ ] Read config file to grab stock list to track.
- [ ] Link to discord bot to send notifications:
  - [ ] Status update channel:
    - [ ] Connection status
    - [ ] Session summary:
      - [ ] Stock symbol
      - [ ] Session highest & lowest price
      - [ ] Deviation from historical highest
  - [ ] Stop hit channel:
    - [ ] Message price
    - [ ] Discord call price (emergency)
