import discord
import datetime

from discord.ext import tasks
from discord_config import channel_id, TOKEN

intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    update_ip_addr.start()

@client.event
async def on_message(message):
    print(f"{message.channel}: {message.author}: {message.content}")
    if message.author == client.user:
        return

    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')
        
@tasks.loop(seconds=10)
async def update_ip_addr():
    channel = client.get_channel(channel_id)
    print(datetime.datetime.now())
    await channel.send(datetime.datetime.now())

client.run(TOKEN)